package com.energy.management.controller;

import com.energy.management.Response;
import com.energy.management.service.AdminServiceImplementation;
import com.energy.management.util.JwtUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.energy.management.entity.Admin;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminServiceImplementation adminService;

    @Autowired
    private JwtUtility jwtUtility;


    @PostMapping("/login")
    public ResponseEntity<Response> adminLogin(@RequestBody Admin admin) {
        Response response = new Response();
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("jwttoken",jwtUtility.generateToken(admin, 10*60*60));
            response.setData(this.adminService.adminLogin(admin));
            return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
        } catch(Exception e) {
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("/addAdmin")
    public ResponseEntity<Response> addAdmin(@RequestBody Admin admin, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.adminService.addAdmin(admin));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("getAll")
    public ResponseEntity<Response> getAll(@RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.adminService.getAll());
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


}
