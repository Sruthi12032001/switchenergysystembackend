package com.energy.management.controller;

import com.energy.management.Response;
import com.energy.management.service.CalculationServiceImplementation;
import com.energy.management.util.JwtUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/calculate")
public class CalculationController {

    @Autowired
    private CalculationServiceImplementation calculationServiceImplementation;

    @Autowired
    private JwtUtility jwtUtility;
    @GetMapping("/{id}")
    public ResponseEntity<Response> calculate(@PathVariable String id, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth)) {
            try{
                response.setData(this.calculationServiceImplementation.calculate(id));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }
}
