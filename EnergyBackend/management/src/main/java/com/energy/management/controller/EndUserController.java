package com.energy.management.controller;

import com.energy.management.Response;
import com.energy.management.dao.EndUserDAOImplementation;
import com.energy.management.entity.EndUser;
import com.energy.management.service.EndUserServiceImplementation;
import com.energy.management.util.JwtUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class EndUserController {
    @Autowired
    private EndUserDAOImplementation endUserDAOImplementation;
    @Autowired
    private EndUserServiceImplementation endUserServiceImplementation;

    @Autowired
    private JwtUtility jwtUtility;

    @PostMapping("/addUser")
    public ResponseEntity<Response> addUser(@RequestBody EndUser endUser, @RequestHeader(value = "authorization") String auth) {

        Response response = new Response();
        if(jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.endUserServiceImplementation.addUser(endUser));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<Response> loginUser(@RequestBody EndUser endUser) {
        Response response = new Response();
        try{
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("jwttoken",jwtUtility.generateToken(endUser, 10*60*60));
            response.setData(this.endUserServiceImplementation.userLogin(endUser));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<Response> getAll(@RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(jwtUtility.validateAdminToken(auth)) {
            try{
                response.setData(this.endUserServiceImplementation.getAll());
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }


    @GetMapping("/getById/{id}")
    public ResponseEntity<Response> getById(@PathVariable String id, @RequestHeader(value = "authorization") String auth) {
            Response response = new Response();
            if(this.jwtUtility.validateEndUserToken(auth)) {
                try{
                    response.setData(this.endUserServiceImplementation.getById(id));
                    return new ResponseEntity<>(response, HttpStatus.OK);
                } catch (Exception e) {
                    response.setMessage(e.getMessage());
                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
    }

    @GetMapping("/getAllSmartMeters/{id}")
    public ResponseEntity<Response> getAllSmartMeters(@PathVariable String id, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth)) {
            try{
                response.setData(this.endUserServiceImplementation.getAllSmartMeters(id));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
