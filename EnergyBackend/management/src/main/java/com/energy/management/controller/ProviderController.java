package com.energy.management.controller;

import com.energy.management.Response;
import com.energy.management.entity.Providers;
import com.energy.management.service.ProviderServiceImplementation;
import com.energy.management.util.JwtUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/provider")
public class ProviderController {
    @Autowired
    private ProviderServiceImplementation providerService;

    @Autowired
    private JwtUtility jwtUtility;


    //user, admin login
    @GetMapping()
    public ResponseEntity<Response> getAllProviders(@RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth) || this.jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.providerService.getProviders());
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }


    @PutMapping("/changeProviderStatus/{id}")
    public ResponseEntity<Response> changeStatus(@RequestBody Providers providers, @PathVariable String id, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.providerService.changeProviderStatus(id, providers));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }


    }

    @PostMapping("/addProvider")
    public ResponseEntity<Response> addProvider(@RequestBody Providers providers, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.providerService.addProvider(providers));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/getProviderByName/{name}")
    public ResponseEntity<Response> getProviderByName(@PathVariable String name, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth)) {
            try{
                response.setData(this.providerService.getProviderByName(name));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
