package com.energy.management.controller;

import com.energy.management.Response;
import com.energy.management.entity.Providers;
import com.energy.management.entity.SmartMeters;
import com.energy.management.service.SmartMeterServiceImplementation;
import com.energy.management.util.JwtUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/smartMeter")
public class SmartMeterController {
    @Autowired
    private SmartMeterServiceImplementation smartMeterService;
    @Autowired
    private JwtUtility jwtUtility;

    @GetMapping("/smartMetersEnrolled")
    public ResponseEntity<Response> getSmartMetersEnrolled(@RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.smartMeterService.getSmartMetersEnrolled());
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/changeSmartMeterStatus/{id}")
    public ResponseEntity<Response> changeStatus(@RequestBody SmartMeters smartMeters, @PathVariable String id, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.smartMeterService.changeSmartMeterStatus(id, smartMeters));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/newSmartMeters")
    public ResponseEntity<Response> newSmartMeters(@RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.smartMeterService.newSmartMeters());
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/addSmartMeter")
    public ResponseEntity<Response> addSmartMeter(@RequestBody SmartMeters smartMeter, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.smartMeterService.addSmartMeter(smartMeter));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }


    }

    @PostMapping("/addNewSmartMeter")
    public ResponseEntity<Response> addNewSmartMeter(@RequestBody SmartMeters smartMeters, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth)) {
            try{
                response.setData(this.smartMeterService.addNewSmartMeter(smartMeters));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/deleteSmartUser/{id}")
    public ResponseEntity<Response> deleteSmartMeter(@PathVariable String id, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateAdminToken(auth)) {
            try {
                response.setData(this.smartMeterService.deleteSmartMeter(id));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setData(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/readings/{id}/{reading}")
    public ResponseEntity<Response> addReadings(@PathVariable String id, @PathVariable String reading, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth)) {
            try{
                response.setData(this.smartMeterService.addReadings(id, Double.parseDouble(reading)));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/changeProvider/{id}")
    public ResponseEntity<Response> changeProvider(@PathVariable String id, @RequestBody Providers provider, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth)) {
            try{
                response.setData(this.smartMeterService.changeProvider(provider.getName(), id));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getReadings/{id}")
    public ResponseEntity<Response> getReadings(@PathVariable String id, @RequestHeader(value = "authorization") String auth) {
        Response response = new Response();
        if(this.jwtUtility.validateEndUserToken(auth)) {
            try{
                response.setData(this.smartMeterService.getReadings(id));
                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (Exception e) {
                response.setMessage(e.getMessage());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


}
