package com.energy.management.dao;

import com.energy.management.entity.Admin;

import java.util.List;

public interface AdminDAO {
    public List<Admin> adminLogin(String mailId);
    public Admin addAdmin(Admin admin);
}
