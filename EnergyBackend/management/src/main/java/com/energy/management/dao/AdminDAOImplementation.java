package com.energy.management.dao;

import com.energy.management.entity.Admin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdminDAOImplementation implements AdminDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private static final Logger logger = LogManager.getLogger(AdminDAOImplementation.class);

    public List<Admin> adminLogin(String mailId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("mail").is(mailId));
        return mongoTemplate.find(query, Admin.class);
    }

    public List<Admin> find(Admin admin) {
        Query query = new Query();
        query.addCriteria(Criteria.where("mail").is(admin.getMail()));
        return this.mongoTemplate.find(query, Admin.class);
    }


    public Admin addAdmin(Admin admin) {
        return mongoTemplate.save(admin);
    }

    public List<Admin> getAll() {
        return this.mongoTemplate.findAll(Admin.class);
    }

//    public List<Admin> getByMailId(String mail) {
//        Query query = new Query();
//        query.addCriteria(Criteria.where("mail").is(mail));
//        return this.mongoTemplate.find(query, Admin.class);
//    }
}
