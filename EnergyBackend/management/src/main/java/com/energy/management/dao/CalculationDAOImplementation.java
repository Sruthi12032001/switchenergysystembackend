package com.energy.management.dao;

import com.energy.management.entity.SmartMeters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CalculationDAOImplementation {
    @Autowired
    private MongoTemplate mongoTemplate;

    public List<SmartMeters> getSmartMeter(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("smart_meter_id").is(id));
        return this.mongoTemplate.find(query, SmartMeters.class);
    }
}
