package com.energy.management.dao;

import com.energy.management.entity.EndUser;

public interface EndUserDAO {
    public EndUser addUser(EndUser endUser);
    public EndUser getEndUserById(String id);
}
