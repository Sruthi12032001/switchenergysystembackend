package com.energy.management.dao;

import com.energy.management.entity.EndUser;
import com.energy.management.entity.SmartMeters;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Repository
public class EndUserDAOImplementation implements EndUserDAO {
    @Autowired
    private MongoTemplate mongoTemplate;

    public EndUser addUser(EndUser endUser) {
        return this.mongoTemplate.save(endUser);
    }
    public EndUser getEndUserById(String id) {
        return this.mongoTemplate.findById(id, EndUser.class);
    }

    public UpdateResult addSmartMeterToEndUser(String id, String smartMeterId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Update update = new Update();
        update.push("smartMeters").each(smartMeterId);
        return this.mongoTemplate.updateFirst(query, update, EndUser.class);
    }

    public List<EndUser> userLogin(String mailid) {
        Query query = new Query();
        query.addCriteria(Criteria.where("mailid").is(mailid));
        return this.mongoTemplate.find(query, EndUser.class);
    }

    public List<EndUser> getAll() {
        return this.mongoTemplate.findAll(EndUser.class);
    }

    public List<EndUser> getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        return this.mongoTemplate.find(query, EndUser.class);
    }

    public List<EndUser> getByMail(String mailId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("mailId").is(mailId));
        return this.mongoTemplate.find(query, EndUser.class);
    }

}
