package com.energy.management.dao;

import com.energy.management.entity.Providers;

import java.util.List;

public interface ProviderDAO {
    public List<Providers> getProviders();
    public Providers changeProviderStatus(String id, Providers providers);
    public Providers addProvider(Providers providers);
}
