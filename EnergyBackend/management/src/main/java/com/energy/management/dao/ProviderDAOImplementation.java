package com.energy.management.dao;

import com.energy.management.entity.Providers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProviderDAOImplementation implements ProviderDAO {
    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Providers> getProviders() {
        return  mongoTemplate.findAll(Providers.class);
    }

    public Providers changeProviderStatus(String id, Providers providers) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Providers provider = this.mongoTemplate.findOne(query, Providers.class);
        provider.setEnabled(providers.isEnabled());
        return this.mongoTemplate.save(provider);
    }

    public Providers addProvider(Providers providers) {
        return this.mongoTemplate.save(providers);
    }

    public List<Providers> getProviderByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return this.mongoTemplate.find(query, Providers.class);
    }
}
