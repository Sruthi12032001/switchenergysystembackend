package com.energy.management.dao;

import com.energy.management.entity.SmartMeters;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface SmartMeterDAO {
    public List<SmartMeters> getSmartMetersEnrolled();

    public SmartMeters changeSmartMeterStatus(String id, SmartMeters smartMeters);

    public List<SmartMeters> newSmartMeters();
    public List<SmartMeters> addSmartMeter(SmartMeters smartMeters);

    public List<SmartMeters> deleteSmartMeter(String id);
}
