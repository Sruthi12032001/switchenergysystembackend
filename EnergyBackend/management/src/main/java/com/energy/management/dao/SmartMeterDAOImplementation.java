package com.energy.management.dao;

import com.energy.management.entity.Providers;
import com.energy.management.entity.SmartMeters;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SmartMeterDAOImplementation implements SmartMeterDAO {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired EndUserDAOImplementation endUserDAOImplementation;

    public List<SmartMeters> getSmartMetersEnrolled() {
        Query query = new Query();
        query.addCriteria(Criteria.where("status").ne("New"));
        return this.mongoTemplate.find(query, SmartMeters.class);
    }

    public SmartMeters changeSmartMeterStatus(String id, SmartMeters smartMeters) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        SmartMeters smartMeter = this.mongoTemplate.findOne(query, SmartMeters.class);
        smartMeter.setStatus(smartMeters.getStatus());
        return this.mongoTemplate.save(smartMeter);
    }

    public List<SmartMeters> newSmartMeters() {
        Query query = new Query();
        query.addCriteria(Criteria.where("status").is("New"));
        return this.mongoTemplate.find(query, SmartMeters.class);
    }

    public List<SmartMeters> addSmartMeter(SmartMeters smartMeters) {
        smartMeters.setStatus("Enabled");
        this.mongoTemplate.save(smartMeters);
        this.endUserDAOImplementation.addSmartMeterToEndUser(smartMeters.getEndUserId(), smartMeters.getSmartMeterId());
        return newSmartMeters();
    }

    public List<SmartMeters> deleteSmartMeter(String id) {
        this.mongoTemplate.remove(new Query().addCriteria(Criteria.where("id").is(id)), SmartMeters.class);
        return newSmartMeters();
    }


    public List<SmartMeters> getBySmartMeterId(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("smart_meter_id").is(id));
        return this.mongoTemplate.find(query, SmartMeters.class);
    }

    public UpdateResult addReadings(String id, Double reading) {
        List<Double> result = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where("smart_meter_id").is(id));
        Update update = new Update();
        update.push("data",reading);
        return this.mongoTemplate.updateFirst(query, update, SmartMeters.class);
    }

    public SmartMeters changeProvider(String name, String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        List<Providers> provider = this.mongoTemplate.find(query, Providers.class);

        if(provider.size() > 0) {
            Query query1 = new Query();
            query1.addCriteria(Criteria.where("smart_meter_id").is(id));
            SmartMeters smartMeters = this.mongoTemplate.find(query1, SmartMeters.class).get(0);
            smartMeters.setProvider(name);
            return this.mongoTemplate.save(smartMeters);
        } else {
            throw new RuntimeException("Provider not found");
        }

    }

    public List<SmartMeters> getReadings(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("smart_meter_id").is(id));
        return this.mongoTemplate.find(query, SmartMeters.class);
    }

    public SmartMeters addNewSmartMeter(SmartMeters smartMeters) {
        return this.mongoTemplate.save(smartMeters);
    }
}
