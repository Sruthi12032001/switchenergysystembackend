package com.energy.management.service;

import com.energy.management.entity.Admin;

import java.util.List;

public interface AdminService{

    public List<Admin> adminLogin(Admin admin);
    public Admin addAdmin(Admin admin);

}
