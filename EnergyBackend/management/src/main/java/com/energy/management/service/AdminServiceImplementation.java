package com.energy.management.service;

import com.energy.management.dao.AdminDAOImplementation;
import com.energy.management.entity.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdminServiceImplementation implements AdminService{
    @Autowired
    private AdminDAOImplementation adminDAOImplementation;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<Admin> adminLogin(Admin admin) {
        List<Admin> list = this.adminDAOImplementation.adminLogin(admin.getMail());
        if(list.size() > 0) {
            if(passwordEncoder.matches(admin.getPassword(), list.get(0).getPassword())) {
                return list;
            } else {
                throw new RuntimeException("Enter valid password");
            }
        } else {
            throw new RuntimeException("Enter valid mail id");
        }

    }

    public Admin addAdmin(Admin admin) {
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        if(this.adminDAOImplementation.find(admin).size() > 0) {
            throw new RuntimeException("Admin already found");
        } else {
            return this.adminDAOImplementation.addAdmin(admin);

        }

    }

    public List<Admin> getAll() {
        return this.adminDAOImplementation.getAll();
    }

//    public List<Admin> getByMailId(String mail) {
//        return this.adminDAOImplementation.getByMailId(mail);
//    }
}
