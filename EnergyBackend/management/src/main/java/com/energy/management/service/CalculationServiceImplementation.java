package com.energy.management.service;

import com.energy.management.dao.CalculationDAOImplementation;
import com.energy.management.entity.SmartMeters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalculationServiceImplementation {
    @Autowired
    private CalculationDAOImplementation calculationDAOImplementation;
    public double calculate(String id) {
        SmartMeters smartMeter = this.calculationDAOImplementation.getSmartMeter(id).get(0);
        Double totalKW = 0.0;
        Double totalKWH = 0.0;
        try {
            for(Double reading: smartMeter.getData()) {
                totalKW = totalKW + reading;
            }
            totalKWH = totalKW * ( smartMeter.getData().length/ 60.0);
        } catch (Exception e) {
            smartMeter.setData(new double[]{0});
        }
        return totalKWH;
    }
}
