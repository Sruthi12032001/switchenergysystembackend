package com.energy.management.service;

import com.energy.management.entity.Admin;
import com.energy.management.entity.EndUser;

public interface EndUserService {

    public EndUser addUser(EndUser endUser);
}
