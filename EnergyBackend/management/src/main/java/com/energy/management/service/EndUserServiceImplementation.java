package com.energy.management.service;

import com.energy.management.dao.EndUserDAOImplementation;
import com.energy.management.dao.SmartMeterDAOImplementation;
import com.energy.management.entity.EndUser;
import com.energy.management.entity.SmartMeters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EndUserServiceImplementation {
    @Autowired
    private EndUserDAOImplementation endUserDAOImplementation;

    @Autowired
    private SmartMeterDAOImplementation smartMeterDAOImplementation;

    @Autowired
    private PasswordEncoder passwordEncoder;



    public EndUser addUser(EndUser endUser) {
        if(this.endUserDAOImplementation.getByMail(endUser.getMailId()).size() > 0) {
            throw new RuntimeException("User Already found");
        } else {
            endUser.setPassword(passwordEncoder.encode(endUser.getPassword()));
            return this.endUserDAOImplementation.addUser(endUser);
        }
    }

    public long addSmartMeterToEndUser(String id, String smartMeterId) {
        return this.endUserDAOImplementation.addSmartMeterToEndUser(id, smartMeterId).getModifiedCount();
    }

    public List<EndUser> userLogin(EndUser endUser) {
        List<EndUser> list = this.endUserDAOImplementation.userLogin(endUser.getMailId());
        if(list.size() > 0) {
            if(passwordEncoder.matches(endUser.getPassword(),list.get(0).getPassword())) {
                return list;
            } else {
                throw new RuntimeException("Enter valid password");
            }
        } else {
            throw new RuntimeException("Enter valid mail id");
        }
    }

    public List<EndUser> getAll() {
        return this.endUserDAOImplementation.getAll();
    }

    public EndUser getById(String id) {
        return this.endUserDAOImplementation.getById(id).get(0);
    }

    public List<SmartMeters> getAllSmartMeters(String id) {
        List<SmartMeters> list = new ArrayList<>();
        List<EndUser> user = this.endUserDAOImplementation.getById(id);
        if(user.size() > 0) {
            System.out.println(user.get(0).getSmartMeters().length);
            for(String id1: user.get(0).getSmartMeters()){
                System.out.println(id1);
                System.out.println(this.smartMeterDAOImplementation.getBySmartMeterId(id1));
                list.add(this.smartMeterDAOImplementation.getBySmartMeterId(id1).get(0));
            }
            return list;
        } else {
            throw new RuntimeException("Id not found");
        }
    }

}
