package com.energy.management.service;

import com.energy.management.entity.Providers;

import java.util.List;

public interface ProviderService {
    public List<Providers> getProviders();
    public Providers changeProviderStatus(String id, Providers providers);
    public Providers addProvider(Providers providers);
}
