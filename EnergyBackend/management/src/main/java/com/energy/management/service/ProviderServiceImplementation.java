package com.energy.management.service;

import com.energy.management.dao.ProviderDAOImplementation;
import com.energy.management.entity.Providers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderServiceImplementation implements ProviderService{
    @Autowired
    private ProviderDAOImplementation providerDAOImplementation;
    private static final Logger logger = LogManager.getLogger(ProviderServiceImplementation.class);

    public List<Providers> getProviders() {
        List<Providers> list = this.providerDAOImplementation.getProviders();
        if(list.size() > 0) {
            return list;
        } else {
            throw new RuntimeException("Data not found");
        }
    }

    public Providers changeProviderStatus(String id, Providers providers) {
        return this.providerDAOImplementation.changeProviderStatus(id, providers);
    }
    public Providers addProvider(Providers providers) {
        if(this.providerDAOImplementation.getProviderByName(providers.getName()).size() > 0) {
            throw new RuntimeException("Provider already found");
        }
        return this.providerDAOImplementation.addProvider(providers);
    }


    public List<Providers> getProviderByName(String name) {
        System.out.println(name);
        return this.providerDAOImplementation.getProviderByName(name);
    }
}
