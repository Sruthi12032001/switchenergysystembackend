package com.energy.management.service;

import com.energy.management.entity.SmartMeters;

import java.util.List;

public interface SmartMeterService {
    public List<SmartMeters> getSmartMetersEnrolled();
    public SmartMeters changeSmartMeterStatus(String id, SmartMeters smartMeters);
    public List<SmartMeters> newSmartMeters();
    public List<SmartMeters> addSmartMeter(SmartMeters newSmartMeter);
    public List<SmartMeters> deleteSmartMeter(String id);
}
