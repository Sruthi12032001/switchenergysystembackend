package com.energy.management.service;

import com.energy.management.dao.SmartMeterDAOImplementation;
import com.energy.management.entity.SmartMeters;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SmartMeterServiceImplementation implements SmartMeterService{

    @Autowired
    private SmartMeterDAOImplementation smartMeterDAOImplementation;
    public List<SmartMeters> getSmartMetersEnrolled() {
        return this.smartMeterDAOImplementation.getSmartMetersEnrolled();
    }


    public SmartMeters changeSmartMeterStatus(String id, SmartMeters smartMeters) {
        return this.smartMeterDAOImplementation.changeSmartMeterStatus(id, smartMeters);
    }

    public List<SmartMeters> newSmartMeters() {
        return this.smartMeterDAOImplementation.newSmartMeters();
    }

    public List<SmartMeters> addSmartMeter(SmartMeters newSmartMeter) {
        return this.smartMeterDAOImplementation.addSmartMeter(newSmartMeter);
    }

    public List<SmartMeters> deleteSmartMeter(String id) {
        return this.smartMeterDAOImplementation.deleteSmartMeter(id);
    }

//    public List<SmartMeters> getBySmartMeterId(String id) {
//        return this.smartMeterDAOImplementation.getBySmartMeterId(id);
//    }

    public UpdateResult addReadings(String id, Double reading) {
        return this.smartMeterDAOImplementation.addReadings(id, reading);
    }

    public SmartMeters changeProvider(String name, String id) {
        return this.smartMeterDAOImplementation.changeProvider(name, id);
    }

    public double[] getReadings(String id) {
        return this.smartMeterDAOImplementation.getReadings(id).get(0).getData();
    }

    public SmartMeters addNewSmartMeter(SmartMeters smartMeters) {
        if(this.smartMeterDAOImplementation.getBySmartMeterId(smartMeters.getSmartMeterId()).size() > 0) {
            throw new RuntimeException("Smart meter id already found");
        } else {
            smartMeters.setStatus("New");
            return this.smartMeterDAOImplementation.addNewSmartMeter(smartMeters);
        }
    }
}
